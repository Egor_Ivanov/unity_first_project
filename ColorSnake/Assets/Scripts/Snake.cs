﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    [SerializeField] private GameController m_gameController;
    [SerializeField] private SpriteRenderer m_spriteRenderer;
    
    private Vector3 position;
    private int currentType;

    private void Start()
    {
        position = transform.position;

        var colorType = m_gameController.Types.GetRandomColorType();
        currentType = colorType.Id;
        m_spriteRenderer.color = colorType.Color;
    }

    void Update()
    {
        position = transform.position;
        if (!Input.GetMouseButton(0))
        {
            return;
        }

        position.x = m_gameController.MainCamera.ScreenToWorldPoint(Input.mousePosition).x;

        float min = m_gameController.Bounds.Left;
        float max = m_gameController.Bounds.Right;

        position.x = Mathf.Clamp(position.x, min, max);
        transform.position = position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var obstacle = other.gameObject.GetComponent<Obstacle>();
        if (!obstacle)
        {
            return;
        }

        SetColor(obstacle.ColorId);
        Destroy(obstacle.gameObject);
    }

    private void SetColor(int id)
    {
        var colorType = m_gameController.Types.GetColorType(id);
        currentType = colorType.Id;
        m_spriteRenderer.color = colorType.Color;
    }
}
