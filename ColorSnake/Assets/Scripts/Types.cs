﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class ColorType
{
    public string Name;
    public int Id;
    public Color Color;
}

[Serializable]
public class ObjectType
{
    public string Name;
    public int Id;
    public GameObject Object;
}

[Serializable]
public class TemplateType
{
    public string Name;
    public int Id;
    public Transform[] Points;
}

public class Types : MonoBehaviour
{
    [SerializeField] private ColorType[] m_colors;
    [SerializeField] private ObjectType[] m_objects;
    [SerializeField] private TemplateType[] m_templates;

    public ColorType GetRandomColorType()
    {
        var rand = Random.Range(0, m_colors.Length);

        return m_colors[rand];
    }
    
    public ObjectType GetRandomObjectType()
    {
        var rand = Random.Range(0, m_objects.Length);

        return m_objects[rand];
    }
    
    public TemplateType GetRandomTemplate()
    {
        var rand = Random.Range(0, m_templates.Length);

        return m_templates[rand];
    }

    public ColorType GetColorType(int id)
    {
        return m_colors.FirstOrDefault(x => x.Id == id);
    }

    public ObjectType GetObjectType(int id)
    {
        return m_objects.FirstOrDefault(x => x.Id == id);
    }
}
