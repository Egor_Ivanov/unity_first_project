using UnityEngine;

public class GameController : MonoBehaviour
{
    public class CameraBounds
    {
        public float Left;
        public float Right;
        public float Up;
        public float Down;
    }
    
    [SerializeField] private Camera m_mainCamera;
    [SerializeField] private Snake m_snake;
    [SerializeField] private Types m_types;

    public Camera MainCamera => m_mainCamera;
    public Types Types => m_types;
    public CameraBounds Bounds { get; private set; }

    private void Awake()
    {
        Vector2 minScreen = m_mainCamera.ScreenToWorldPoint(Vector3.zero);
        Bounds = new CameraBounds
        {
            Left = minScreen.x,
            Right = Mathf.Abs(minScreen.x),
            Up = Mathf.Abs(minScreen.y),
            Down = minScreen.y
        };
    }

    private void Update()
    {
        Vector3 movement = Time.deltaTime * 2f * Vector3.up;
        m_mainCamera.transform.Translate(movement);
        m_snake.transform.Translate(movement);
    }

    private void Reset()
    {
        m_mainCamera = Camera.main;
    }
}
