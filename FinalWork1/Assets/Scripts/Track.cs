﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Track : MonoBehaviour
{
    public float PlatformsDistance = 2f;

    [SerializeField] private GameObject m_gate;
    [SerializeField] private int m_platformsTotalCount = 10;
    [SerializeField] private Button m_Button;

    public bool Started;

    private int delta = 0;

    private readonly List<GameObject> platforms = new List<GameObject>();

    public bool IsBallLandNextPlatform(Vector3 position)
    {
        var nextPlatform = platforms[1];
        var minX = nextPlatform.transform.position.x - 0.07f;
        var maxX = nextPlatform.transform.position.x + 0.07f;
        
        return position.x > minX && position.x < maxX;
    }

    public void ActivateNextPlatform()
    {
        var nextPlatform = platforms[1];
        nextPlatform.GetComponent<Gate>().ActivatePlatform();
        
        var currentPlatform = platforms[0];
        platforms.Remove(currentPlatform);
        SetPlatformPosition(currentPlatform);
        platforms.Add(currentPlatform);
    }

    private void Start()
    {
        m_Button.onClick.AddListener(Starts);
    }

    private void Starts()
    {
        m_Button.gameObject.SetActive(false);
        Started = true;

        platforms.Add(m_gate);

        for (var i = 0; i < m_platformsTotalCount; i++)
        {
            var obj = Instantiate(m_gate, transform);
            SetPlatformPosition(obj);
            obj.name = $"Platform_{i + 1}";
            platforms.Add(obj);
        }
    }

    private void SetPlatformPosition(GameObject platform)
    {
        var pos = Vector3.zero;
        var lastPlatform = platforms[platforms.Count - 1];
        pos.z = lastPlatform.transform.position.z + PlatformsDistance;
        var x = Random.Range(-5, 6);
        pos.x = x * 0.1f;

        if (delta < 10)
        {
            delta++;
            pos.y = pos.y + delta * 0.15f;
            if (x > 0)
            {
                lastPlatform.transform.Rotate(Vector3.back * delta);
            }
            
        }
        else
        {
            delta = 0;
        }
        
        platform.transform.position = pos;
    }
}
