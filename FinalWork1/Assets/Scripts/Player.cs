﻿using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private Text m_text;
    [SerializeField] private Text m_score;
     
    [SerializeField] private PlayerInput m_PlayerInput;
    [SerializeField] private Track m_hopTrack;
    [SerializeField] private AnimationCurve m_jumpCurve;
    [SerializeField] private float m_jumpHeight = 1f;
    [SerializeField] private float m_ballSpeed = 1f;

    private float jumpDistance;
    private float iteration;
    private float startZ;
    private int score;
    private int leftTries = 3;

    private void Start()
    {
        jumpDistance = m_hopTrack.PlatformsDistance;
        m_text.text = "Tries = " +  leftTries;
    }

    void Update()
    {
        if (!m_hopTrack.Started)
        {
            return;
        }
        
        var pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, m_PlayerInput.Strafe, Time.deltaTime * 5f);
        pos.y = m_jumpCurve.Evaluate(iteration) * m_jumpHeight;
        pos.z = startZ + iteration * jumpDistance;
        transform.position = pos;

        iteration += Time.deltaTime * m_ballSpeed;
        if (iteration < 1f)
        {
            return;
        }

        iteration = 0;
        startZ = pos.z;

        if (m_hopTrack.IsBallLandNextPlatform(transform.position))
        {
            m_hopTrack.ActivateNextPlatform();
            score++;
            m_score.text = "Score = " +  score;

            if (score == 10)
            {
                m_score.text = "You won!!!";
            }
            
            return;
        }

        if (leftTries > 0)
        {
            leftTries--;
            m_text.text = "Tries = " +  leftTries;
            return;
        }

        leftTries = 3;

        print($"You score is {score}!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
