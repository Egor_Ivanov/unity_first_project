﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] private GameObject m_platform;

    private float rotationAngle = 360f;
    private float speed = 1f;
    private float timer;

    private bool rotate;

    public void ActivatePlatform()
    {
        rotate = true;
    }

    private void Update()
    {
        if (!rotate)
        {
            return;
        }
        
        transform.RotateAround(Vector3.zero, Vector3.forward,
            Time.deltaTime * speed * rotationAngle);

        timer += Time.deltaTime * speed;
        timer = Mathf.Clamp01(timer);

        if (timer < 0.999f)
            return;

        rotate = false;
    }
}
