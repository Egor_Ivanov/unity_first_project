﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HopPlayer : MonoBehaviour
{
    [SerializeField] private HopInput m_hopInput;
    [SerializeField] private HopTrack m_hopTrack;
    [SerializeField] private AnimationCurve m_jumpCurve;
    [SerializeField] private float m_jumpHeight = 1f;
    [SerializeField] private float m_ballSpeed = 1f;

    private float jumpDistance;
    private float iteration;
    private float startZ;
    private int score;

    private void Start()
    {
        jumpDistance = m_hopTrack.PlatformsDistance;
    }

    void Update()
    {
        var pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, m_hopInput.Strafe, Time.deltaTime * 5f);
        pos.y = m_jumpCurve.Evaluate(iteration) * m_jumpHeight;
        pos.z = startZ + iteration * jumpDistance;
        transform.position = pos;

        iteration += Time.deltaTime * m_ballSpeed;
        if (iteration < 1f)
        {
            return;
        }

        iteration = 0;
        startZ = pos.z;

        if (m_hopTrack.IsBallLandNextPlatform(transform.position))
        {
            m_hopTrack.ActivateNextPlatform();
            score++;
            return;
        }

        print($"You score is {score}!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
