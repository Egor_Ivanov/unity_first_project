﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopPlatform : MonoBehaviour
{
    [SerializeField] private GameObject m_platformDefault;
    [SerializeField] private GameObject m_platformActive;

    public void ActivatePlatform()
    {
        m_platformDefault.SetActive(false);
        m_platformActive.SetActive(true);
        
        Invoke(nameof(SetInactivePlatform), 0.2f);
    }

    private void SetInactivePlatform()
    {
        m_platformDefault.SetActive(true);
        m_platformActive.SetActive(false);
    }
}
