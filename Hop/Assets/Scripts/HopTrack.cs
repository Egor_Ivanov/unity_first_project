﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    public float PlatformsDistance = 2f;

    [SerializeField] private GameObject m_platform;
    [SerializeField] private int m_platformsTotalCount = 10;

    private readonly List<GameObject> platforms = new List<GameObject>();

    public bool IsBallLandNextPlatform(Vector3 position)
    {
        var nextPlatform = platforms[1];
        var minX = nextPlatform.transform.position.x - 0.5f;
        var maxX = nextPlatform.transform.position.x + 0.5f;
        
        return position.x > minX && position.x < maxX;
    }

    public void ActivateNextPlatform()
    {
        var nextPlatform = platforms[1];
        nextPlatform.GetComponent<HopPlatform>().ActivatePlatform();

        var currentPlatform = platforms[0];
        platforms.Remove(currentPlatform);
        SetPlatformPosition(currentPlatform);
        platforms.Add(currentPlatform);
    }

    private void Start()
    {
        platforms.Add(m_platform);

        for (var i = 0; i < m_platformsTotalCount; i++)
        {
            var obj = Instantiate(m_platform, transform);
            SetPlatformPosition(obj);
            obj.name = $"Platform_{i + 1}";
            platforms.Add(obj);
        }
    }

    private void SetPlatformPosition(GameObject platform)
    {
        var pos = Vector3.zero;
        var lastPlatform = platforms[platforms.Count - 1];
        pos.z = lastPlatform.transform.position.z + PlatformsDistance;
        pos.x = Random.Range(-1, 2);
        platform.transform.position = pos;
    }
}
