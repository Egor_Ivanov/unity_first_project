﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverCamera : MonoBehaviour
{
    [SerializeField] private Transform m_carTransform;
    
    private float camZ;
    
    private void Start()
    {
        camZ = transform.position.z;
    }

    private void Update()
    {
        Vector3 pos = m_carTransform.position;
        pos.z = camZ;
        transform.position = pos;
    }
}
