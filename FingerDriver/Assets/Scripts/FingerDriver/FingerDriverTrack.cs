﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverTrack : MonoBehaviour
{
    [SerializeField] private LineRenderer m_lineRenderer;
    [SerializeField] private bool m_debug;

    private Vector3[] corners;
    private TrackSegment[] segments;
    private int playerCurrentSegmentIndex;

    public bool IsPointInTrack(Vector3 point)
    {
        if (segments[playerCurrentSegmentIndex].IsPointInSegment(point))
        {
            return true;
        }
        else if (playerCurrentSegmentIndex < segments.Length - 1 && segments[playerCurrentSegmentIndex + 1].IsPointInSegment(point))
        {
            playerCurrentSegmentIndex++;
            return true;
        }
        else if (playerCurrentSegmentIndex < segments.Length - 2 && segments[playerCurrentSegmentIndex + 2].IsPointInSegment(point))
        {
            playerCurrentSegmentIndex += 2;
            return true;
        }
        else if (playerCurrentSegmentIndex > 0 && segments[playerCurrentSegmentIndex - 1].IsPointInSegment(point))
        {
            playerCurrentSegmentIndex--;
            return true;
        }

        return false;
    }

    public float Progress => playerCurrentSegmentIndex / 2;
    
    void Start()
    {
        corners = new Vector3[transform.childCount];
        for (var i = 0; i < corners.Length; i++)
        {
            var obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            obj.GetComponent<MeshRenderer>().enabled = false;
        }

        m_lineRenderer.positionCount = corners.Length;
        m_lineRenderer.SetPositions(corners);
        
        var mesh = new Mesh();
        m_lineRenderer.BakeMesh(mesh, true);

        segments = new TrackSegment[mesh.triangles.Length / 3];
        var segmentCounter = 0;
        for (var i = 0; i < mesh.triangles.Length; i += 3)
        {
            var points = new Vector3[3];
            points[0] = mesh.vertices[mesh.triangles[i]];
            points[1] = mesh.vertices[mesh.triangles[i + 1]];
            points[2] = mesh.vertices[mesh.triangles[i + 2]];
            segments[segmentCounter] = new TrackSegment(points);
            segmentCounter++;
        }

        if (!m_debug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = point;
                sphere.transform.localScale = Vector3.one * 0.1f;
            }
        }
    }

    private class TrackSegment
    {
        public readonly Vector3[] Points;

        public TrackSegment(Vector3[] points)
        {
            Points = points;
        }

        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point, Points[0], Points[1], Points[2]);
        }
    }
}
