﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FingerDriverPlayer : MonoBehaviour
{
    [SerializeField] private FingerDriverTrack m_track;
    [SerializeField] private FingerDriverInput m_input;
    [SerializeField] private Transform m_trackingPoint;
    [SerializeField] private float m_carSpeed = 2f;
    [SerializeField] private float m_maxSteer = 90f;

    private void Update()
    {
        if (m_track.IsPointInTrack(m_trackingPoint.position))
        {
            transform.Translate(transform.up * Time.deltaTime * m_carSpeed, Space.World);
            transform.Rotate(0f, 0f, m_maxSteer * m_input.SteerAxis * Time.deltaTime);
        }
        else
        {
            print($"Your Score is {m_track.Progress}.");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
            
    }
}
