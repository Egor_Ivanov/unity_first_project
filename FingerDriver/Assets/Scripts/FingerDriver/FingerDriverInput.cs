﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverInput : MonoBehaviour
{
    [SerializeField] [Range(0f, 180f)] private float m_maxSteerAngle = 90;
    [SerializeField] [Range(0f, 1f)] private float m_steerAcceleration = 0.25f;

    private Vector2 startSteerWheelPoint;
    private Camera mainCamera;
    private float steerAxis;

    public float SteerAxis
    {
        get => steerAxis;
        set => steerAxis = Mathf.Lerp(steerAxis, value, m_steerAcceleration);
    }
     
    void Start()
    {
        mainCamera = Camera.main;
        startSteerWheelPoint = mainCamera.WorldToScreenPoint(transform.position);
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            float anlge = Vector2.Angle(Vector2.up, (Vector2) Input.mousePosition - startSteerWheelPoint);

            anlge /= m_maxSteerAngle;
            anlge = Mathf.Clamp01(anlge);

            if (Input.mousePosition.x > startSteerWheelPoint.x)
            {
                anlge *= -1f;
            }

            SteerAxis = anlge;
        }
        else
        {
            SteerAxis = 0;
        }
        
        transform.localEulerAngles = new Vector3(0f, 0f, SteerAxis * m_maxSteerAngle);
    }
}
