﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall
{
    private readonly List<Transform> cubes = new List<Transform>();
    private Transform parent;

    public Transform Parent => parent;

    public Wall(int sizeX, int sizeY, GameObject prefab)
    {
        GenerateWall(sizeX, sizeY, prefab);
    }

    public void SetupWall(Template template, float position)
    {
        parent.transform.position = new Vector3(0f, 0f, position);
        foreach (var cube in cubes)
        {
            cube.gameObject.SetActive(true);
        }

        if (template == null)
        {
            return;
        }

        Transform[] figure = template.GetFigure();
        for (var f = 0; f < figure.Length; f++)
        {
            for (var c = 0; c < cubes.Count; c++)
            {
                if (!figure[f] || !cubes[c])
                {
                    continue;
                }

                if (Mathf.Abs(figure[f].position.x - cubes[c].position.x) > 0.1f)
                {
                    continue;
                }

                if (Mathf.Abs(figure[f].position.y - cubes[c].position.y) > 0.1f)
                {
                    continue;
                }

                cubes[c].gameObject.SetActive(false);
            }
        }
    }
    
    private void GenerateWall(int sizeX, int sizeY, GameObject prefab)
    {
        parent = new GameObject().transform;

        for (var x = -sizeX + 1; x < sizeX; x++)
        {
            for (var y = 0; y < sizeY; y++)
            {
                var obj = Object.Instantiate(prefab, new Vector3(x, y, 0f), Quaternion.identity);
                obj.transform.parent = parent;
                cubes.Add(obj.transform);
            }
        }

        parent.position = new Vector3(0f, 0.5f, 0f);
    }
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
