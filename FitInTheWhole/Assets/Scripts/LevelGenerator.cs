﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_cubePrefab;
    [SerializeField] private float m_baseSpeed = 15f;
    [SerializeField] private float m_wallDistance = 35f;
    [SerializeField] private Template[] m_templatePrefabs;
    [SerializeField] private Transform m_figurePoint;

    private Template[] templates;
    private Template currentFigure;
    
    private float speed;
    private Wall wall;
    
    void Start()
    {
        templates = new Template[m_templatePrefabs.Length];
        for (int i = 0; i < templates.Length; i++)
        {
            templates[i] = Instantiate(m_templatePrefabs[i]);
            templates[i].gameObject.SetActive(false);
            templates[i].transform.position = m_figurePoint.position;
        }
        
        wall = new Wall(5, 5, m_cubePrefab);
        SetupTemplate();
        wall.SetupWall(currentFigure, m_wallDistance);
        speed = m_baseSpeed;
    }

    void Update()
    {
        wall.Parent.transform.Translate(speed * Time.deltaTime * Vector3.back);
        if (wall.Parent.transform.position.z > m_wallDistance * -1f)
        {
            return;
        }
        
        SetupTemplate();
        wall.SetupWall(currentFigure, m_wallDistance);
    }

    private void SetupTemplate()
    {
        if (currentFigure)
        {
            currentFigure.gameObject.SetActive(false);
        }

        var random = Random.Range(0, templates.Length);
        currentFigure = templates[random];
        currentFigure.gameObject.SetActive(true);
        currentFigure.SetupRandomFigure();
    }
}

