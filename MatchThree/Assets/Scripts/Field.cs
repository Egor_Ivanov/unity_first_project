using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace
{
    public class Field : MonoBehaviour
    {
        [SerializeField] private Camera m_mainCamera;
        [SerializeField] private GameObject m_cell;
        [SerializeField] private float m_cellSize = 0.6f;
        [SerializeField] private int m_fieldWidth = 6;
        [SerializeField] private int m_fieldHeight = 8;
        
        public static float CurrentCellSize;
        
        private static readonly List<List<Cell>> _gameField = new List<List<Cell>>();

        public void Init()
        {
            GenerateField(m_fieldWidth, m_fieldHeight);
            CurrentCellSize = m_cellSize;
        }

        private void GenerateField(int width, int height)
        {
            for (var x = 0; x < width; x++)
            {
                _gameField.Add(new List<Cell>(width));
                for (int y = 0; y < height; y++)
                {
                    Vector3 pos = new Vector3(x * m_cellSize, y * m_cellSize, 0f);
                    var obj = Instantiate(m_cell, pos, Quaternion.identity);
                    obj.name = $"Cell {x} {y}";

                    var cell = obj.AddComponent<Cell>();
                    _gameField[x].Add(cell);

                    if (x > 0)
                    {
                        cell.SetNeighbour(Direction.Left, _gameField[x - 1][y]);
                        _gameField[x - 1][y].SetNeighbour(Direction.Right, cell);
                    }
        
                    if (y > 0)
                    {
                        cell.SetNeighbour(Direction.Down, _gameField[x][y - 1]);
                        _gameField[x][y - 1].SetNeighbour(Direction.Up, cell);
                    }
                }
            }
            
            var cameraPos = new Vector3(width * m_cellSize * 0.5f, height * m_cellSize * 0.5f, -1);
            m_mainCamera.transform.position = cameraPos;
        }

        public static Cell GetCell(Candy candy)
        {
            foreach (var row in _gameField)
            {
                foreach (var cell in row)
                {
                    if (cell.Candy == candy)
                    {
                        return cell;
                    }
                }
            }

            return null;
        }

        public static Cell GetCell(int x, int y)
        {
            return _gameField[x][y];
        }
    }
}