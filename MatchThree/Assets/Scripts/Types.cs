﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Types : MonoBehaviour
{
    [SerializeField] private CandyData[] m_CandiesData;
    public Candy GetRandomCandy()
    {
        CandyData candyData = m_CandiesData[Random.Range(0, m_CandiesData.Length)];
        GameObject obj = Instantiate(candyData.Prefab);
        Candy candy = obj.AddComponent<Candy>();
        candy.CandyData = candyData;
        SpriteRenderer spriteRenderer = obj.GetComponent<SpriteRenderer>();
        spriteRenderer.color = candyData.Color;
        
        return candy;
    }
}
