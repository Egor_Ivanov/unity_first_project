using DefaultNamespace;
using UnityEngine;

public class Candy : MonoBehaviour
{
    public CandyData CandyData;
    private Vector3 baseCandyPosition;
    private bool inDrag;
    private Collider2D currentCollider;

    private void OnMouseDown()
    {
        baseCandyPosition = transform.position;
        inDrag = true;
    }


    private void OnMouseDrag()
    {
        if (!inDrag)
        {
            return;
        }

        var position = Controller.MainCamera.ScreenToWorldPoint(Input.mousePosition);
        position.z = baseCandyPosition.z;
        //максимальное смещение конфеты
        float maxDistance = Field.CurrentCellSize;

        float x = position.x;
        float baseX = baseCandyPosition.x;
        float y = position.y;
        float baseY = baseCandyPosition.y;

        //ограничение смещения до одной ячейки
        x = Mathf.Clamp(x, baseX - maxDistance, baseX + maxDistance);
        y = Mathf.Clamp(y, baseY - maxDistance, baseY + maxDistance);
        //ограничение смещения по одной оси
        if (Mathf.Abs(x - baseX) > Mathf.Abs(y - baseY))
        {
            y = baseY;
        }
        else
        {
            x = baseX;
        }


        position.x = x;
        position.y = y;

        transform.position = position;
    }


    private void OnMouseUp()
    {
        inDrag = false;
        if (currentCollider)
        {
            Candy targetCandy = currentCollider.GetComponent<Candy>();
            var targetCell = Field.GetCell(targetCandy);
            var currentCell = Field.GetCell(this);
            //пытаемся поменять ячейки
            currentCell.Candy = targetCandy;
            targetCell.Candy = this;

            //проверить валидность
            bool isFreePlacement = Controller.IsFreeCandyPlacement(targetCell, CandyData.Id);
            if (isFreePlacement)
            {
                isFreePlacement = Controller.IsFreeCandyPlacement(currentCell, targetCandy.CandyData.Id);
            }

            //если смена НЕ приведет к совпадению 3-х
            if (isFreePlacement)
            {
                targetCell.Candy = targetCandy;
                currentCell.Candy = this;
                transform.position = baseCandyPosition;
                return;
            }

            //Если приведет к совпадению 3-х
            transform.position = targetCell.transform.position;
            targetCandy.transform.position = currentCell.transform.position;
            return;
        }
        
        transform.position = baseCandyPosition;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        currentCollider = other;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (currentCollider == other)
        {
            currentCollider = null;
        }
    }
}
