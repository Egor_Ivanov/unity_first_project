﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Direction
{
    None = -1,
    Left = 0,
    Right = 1,
    Up = 2,
    Down = 3
}

public class Neighbour
{
    public Direction Direction;
    public Cell Cell;
}

public class Cell : MonoBehaviour
{
    public Candy Candy;
    
    private readonly Neighbour[] _neighbours = new Neighbour[4];

    public Cell GetNeighbour(Direction direction)
    {
        return _neighbours.FirstOrDefault(x => x != null && x.Direction == direction)?.Cell;
    }

    public void SetNeighbour(Direction direction, Cell cell)
    {
        if (GetNeighbour(direction))
        {
            return;
        }

        _neighbours[(int) direction] = new Neighbour()
        {
            Direction = direction,
            Cell = cell
        };
    }
}
