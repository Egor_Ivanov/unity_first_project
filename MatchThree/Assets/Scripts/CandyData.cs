﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CandyData
{
    public string Name;
    public int Id;
    public Color Color;
    public GameObject Prefab;
}
