﻿using UnityEngine;

public class StickHeroPlayer : MonoBehaviour
{
    [SerializeField] private StickHeroController m_Controller;
	
    private bool isMoving;
    private float targetPosition;
    private bool isFalling;

    public void StartMovement(float targetPos, bool isFall)
    {
        isMoving = true;
        targetPosition = targetPos;
        isFalling = isFall;
    }
	
    private void Update()
    {
        if (isMoving)
        {
            transform.Translate(Vector3.right * Time.deltaTime * 2f);
            if (transform.position.x < targetPosition) return;

            isMoving = false;
            if (!isFalling)
                m_Controller.StopPlayerMovement();
        }

        if (!isFalling) return;
		
        transform.Translate(Vector3.down * Time.deltaTime * 2f);
        if (transform.position.y <= 0f)
        {
            isFalling = false;
            m_Controller.ShowScores();
        }
    }
}
