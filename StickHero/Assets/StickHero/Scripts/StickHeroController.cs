﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StickHeroController : MonoBehaviour
{
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private StickHeroPlatform m_Platform;
    [SerializeField] private int m_PlatformsTotalCount = 7;
    [SerializeField] private float m_PlatformsMinDistance = 2f;
    [SerializeField] private float m_PlatformsMaxDistance = 7f;
    [SerializeField] private float m_PlatformMinSize = 0.5f;
    [SerializeField] private float m_PlatformMaxSize = 1.3f;

    private readonly List<StickHeroPlatform> platforms = new List<StickHeroPlatform>();
    private EGameState currentGameState;
    private int counter;

    private StickHeroPlatform CurrentPlatform => platforms[0];

    public void StopStickScale()
    {
        currentGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }

    public void StopStickRotate()
    {
        currentGameState = EGameState.Movement;
    }

    public void StartPlayerMovement(float lenght)
    {
        currentGameState = EGameState.Movement;

        var targetPlatform = platforms[1];
        float targetLenght = targetPlatform.transform.position.x - m_Stick.transform.position.x;
        float platformSize = targetPlatform.GetPlatformSize();
        float min = targetLenght - platformSize * 0.5f;
        float max = targetLenght + platformSize * 0.5f;

        if (lenght < min || lenght > max)
        {
            float targetPosition = m_Stick.transform.position.x + lenght;
            m_Player.StartMovement(targetPosition, true);
        }
        else
        {
            float targetPosition = targetPlatform.transform.position.x;
            m_Player.StartMovement(targetPosition, false);
        }
    }

    public void StopPlayerMovement()
    {
        currentGameState = EGameState.Wait;
        counter++;
        ReplaceCurrentPlatform();
        m_Stick.ResetStick(CurrentPlatform.GetStickPosition());
    }

    public void ShowScores()
    {
        currentGameState = EGameState.Defeate;
        print("Game Over");
        print($"Your score : {counter}!");
    }

    private void Start()
    {
        currentGameState = EGameState.Wait;
        CreatePlatforms();
        m_Stick.MaxStickLength = m_PlatformsMaxDistance;
        m_Stick.ResetStick(CurrentPlatform.GetStickPosition());
    }

    private void Update()
    {
        if (!Input.GetMouseButtonDown(0)) return;
        switch (currentGameState)
        {
            case EGameState.Wait:
                currentGameState = EGameState.Scaling;
                m_Stick.StartScaling();
                break;
            case EGameState.Scaling:
                currentGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                break;
            case EGameState.Rotate:
                break;
            case EGameState.Movement:
                break;
            case EGameState.Defeate:
                print("Game restarted");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void CreatePlatforms()
    {
        platforms.Add(m_Platform);
        for (var i = 0; i < m_PlatformsTotalCount - 1; i++)
        {
            var newPlatform = Instantiate(m_Platform);
            SetPlatformTransform(newPlatform);
            platforms.Add(newPlatform);
        }
    }

    private void ReplaceCurrentPlatform()
    {
        var platform = CurrentPlatform;
        platforms.Remove(platform);
        SetPlatformTransform(platform);
        platforms.Add(platform);
    }

    private void SetPlatformTransform(StickHeroPlatform newPlatform)
    {
        var platformSize = UnityEngine.Random.Range(m_PlatformMinSize, m_PlatformMaxSize);
        newPlatform.transform.localScale = new Vector3(platformSize, newPlatform.transform.localScale.y);

        var lastPlatformStickPositonX = platforms[platforms.Count - 1].GetStickPosition().x;
        var platformHalfSize = platformSize * 0.5f;
        var minPositionX = lastPlatformStickPositonX + CalculatePlatformsMinDistance() + platformHalfSize;
        var maxPositionX = lastPlatformStickPositonX + m_PlatformsMaxDistance - platformHalfSize;
        var positionX = UnityEngine.Random.Range(minPositionX, maxPositionX);
        newPlatform.transform.position = new Vector3(positionX, newPlatform.transform.position.y);

        float CalculatePlatformsMinDistance()
        {
            if (platforms.Count == 1)
            {
                return m_PlatformsMinDistance;
            }

            var platformBeforeLast = platforms[platforms.Count - 2];
            var minDistance = m_PlatformsMaxDistance - (lastPlatformStickPositonX - platformBeforeLast.GetStickPosition().x);

            return Math.Max(m_PlatformsMinDistance, minDistance);
        }
    }

    private enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeate
    }
}